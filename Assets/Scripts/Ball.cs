using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Ball : NetworkBehaviour
{

    public float speed = 5f;
    public float maxVelocity = 5f;
    
    public GameObject scriptManager;
    private ScoreManager scoreManager;

    public Rigidbody rb;
    private Vector3 dir;
    
    [SyncVar(hook = nameof(SetOwner))]
    public GameObject owner;

    [SyncVar(hook = nameof(SetMoveBool))]
    public bool canMove = false;

    [SyncVar(hook = nameof(SetColor))]
    public Color ballColor = Color.white;

    
    private void Awake()
    {        
        rb = GetComponent<Rigidbody>();
        scriptManager = GameObject.Find("ScriptManager");
        scoreManager = scriptManager.GetComponent<ScoreManager>();        
    }
    #region Hooks
    public void SetColor(Color oldCol, Color newCol)
    {
        GetComponent<MeshRenderer>().material.color = newCol;
    }

    public void SetOwner(GameObject oldOwner, GameObject newOwner)
    {
        owner = newOwner;
    }

    public void SetMoveBool(bool oldBool, bool newBool)
    {
        canMove = newBool;
    }
    #endregion

    private void FixedUpdate()
    {
        // Only able to move if t he owner ref has been set to avoid errors
        if (owner != null)
            if (!canMove) transform.position = owner.GetComponent<PlayerMovement>().ballSpawnPoint.transform.position;
        
    }    

    public void Death()
    {
        rb.velocity = Vector3.zero;
        canMove = false;
    }

    #region ServerTriggers&Collisions
    [ServerCallback]
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "DeathZone")
        {
            Death();
            Debug.Log("I died");       
        }        
    }

    [ServerCallback]
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {            

            dir = new Vector3(transform.position.x - collision.transform.position.x / collision.collider.bounds.size.x, 1f, 0f).normalized;
        }

        if (collision.gameObject.tag == "Brick")
        {
            Destroy(collision.gameObject);
            scoreManager.UpdateScore();
        }
    }
    #endregion

    #region Playball_replicated
    public void PlayBall()
    {
        if (!canMove)
        {
            canMove = true;

            CmdPlayBall();
        }        
    }

    [Command(requiresAuthority = false)]
    public void CmdPlayBall()
    {
        rb.velocity = new Vector3(Random.Range(-1f, 1f), 1f, 0f) * speed;
    }

    #endregion

    #region OnStop
    public override void OnStopClient()
    {
        base.OnStopClient();
        Destroy(this.gameObject);

    }
    public override void OnStopServer()
    {
        base.OnStopServer();
        Destroy(this.gameObject);
    }
    #endregion
}
