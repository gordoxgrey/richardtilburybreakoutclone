using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class BrickInfo : NetworkBehaviour
{

    [SyncVar(hook = nameof(SetColor))]
    public Color brickColor = Color.white;

    public void SetColor(Color oldCol, Color newCol)
    {
        GetComponent<MeshRenderer>().material.color = newCol;
    }
}
