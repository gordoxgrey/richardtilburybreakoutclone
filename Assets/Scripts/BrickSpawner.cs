using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class BrickSpawner : NetworkBehaviour
{
    public int numRows = 10;
    public int numCols = 5;
    public float xOffset = 1f;
    public float yOffset = 0.5f;
    public GameObject spawnStart;
    public GameObject brickPrefab;
    public GameObject bricksParent;
    public List<Color> colorslist = new List<Color>();
           

    public override void OnStartServer()
    {
        base.OnStartServer();
        SpawnBricks();
    }  

    public void Replay()
    {
        GameObject[] balls;
        balls = GameObject.FindGameObjectsWithTag("Ball");

        foreach (GameObject i in balls)
        {
            i.GetComponent<Ball>().Death();
        }

        SpawnBricks();
    }

    public void SpawnBricks()
    {
        for (int i = 0; i < numRows; i++)
        {
            Vector3 spawnPos = spawnStart.transform.position;

            for (int j = 0; j < numCols; j++)
            {
                Vector3 spacing = new Vector3(xOffset * i, -yOffset * j, 0);
                GameObject brick = Instantiate(brickPrefab, spawnPos + spacing, Quaternion.identity);
                NetworkServer.Spawn(brick);
                brick.GetComponent<BrickInfo>().brickColor = colorslist[j];
                brick.transform.parent = bricksParent.transform;
            }
        }

        this.gameObject.GetComponent<ScoreManager>().Replay();
    }
}
