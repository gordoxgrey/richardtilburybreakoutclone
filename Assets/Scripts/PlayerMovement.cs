using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class PlayerMovement : NetworkBehaviour
{

    //public Camera mainCamera;
    public bool useMouse = false;
    public float speed = 10f;
    private Rigidbody rb;

    public float OffsetFromWall = 2f;
    public GameObject WallLeft;
    public GameObject WallRight;

    // Reference to the Prefab. Drag a Prefab into this field in the Inspector.
    [SerializeField] private GameObject BallPrefab;
    public GameObject ballSpawnPoint;
        
    private Ball ball;

    [SyncVar(hook = nameof(SetColor))]
    public Color playerColor = Color.white;
    
    public void SetColor(Color oldCol, Color newCol)
    {
        GetComponent<MeshRenderer>().material.color = newCol;
    }

    public override void OnStartClient()
    {
        base.OnStartClient();

        RespawnBall();

        WallLeft = GameObject.Find("WallLeft");
        WallRight = GameObject.Find("WallRight");
        rb = GetComponent<Rigidbody>();
    }   
    
    public override void OnStartServer()
    {
        base.OnStartServer();

        playerColor = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
    }

    #region Update
    // Update is called once per frame
    void Update()
    {
        // Locks the paddle to the bounds of the walls with an offset so it doesnt clip the wall
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, WallLeft.transform.position.x + OffsetFromWall, WallRight.transform.position.x - OffsetFromWall), transform.position.y, transform.position.z);

        if (isLocalPlayer)
        {
            
            if (useMouse)
            {
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    PlayBall();
                }
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    PlayBall();
                }
            }
        }        
    }
    #endregion

    #region FixedUpdate
    private void FixedUpdate()
    {
        if (isLocalPlayer)
        {
            Vector3 mousePos = Input.mousePosition;
            // Need to add a z depth beforehand because reasons
            mousePos.z = 1f;
            Vector3 worldPos = Camera.main.ScreenToWorldPoint(mousePos);

            Vector3 pos = Camera.main.WorldToViewportPoint(transform.position);

            if (useMouse)
            {
                if ((pos.x > 0) && (pos.x < 1))
                {
                    transform.position = new Vector3(worldPos.x, 0.5f, 0);
                }
                else
                {
                    transform.position = new Vector3(pos.x, 0.5f, 0);
                }
            }
            else
            {
                //transform.position = new Vector3(transform.position.x + speed * Input.GetAxis("Horizontal"), 0.5f, 0f);            

                rb.velocity = Vector3.right * Input.GetAxis("Horizontal") * speed;

            }
        }        
    }
    #endregion

    #region RespawningOfBall
    public void RespawnBall()
    {
        if (!isLocalPlayer) return;
        CmdRespawnBall();
    }

    // Respawns the ball, acts as an initial spawn too
    [Command]
    public void CmdRespawnBall()
    {
        GameObject ballRef = Instantiate(BallPrefab, ballSpawnPoint.transform.position, Quaternion.identity);
        
        NetworkServer.Spawn(ballRef);

        ball = ballRef.GetComponent<Ball>();

        ballRef.GetComponent<Ball>().owner = gameObject;

        ballRef.GetComponent<Ball>().ballColor = playerColor;
    }
    #endregion

    #region PlayBallMethods
    public void PlayBall()
    {
        if (!isLocalPlayer) return;
        CmdPlayBall();
    }

    [Command]
    public void CmdPlayBall()
    {
        ball.PlayBall();        
    }
    #endregion
}
