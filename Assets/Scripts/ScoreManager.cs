using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class ScoreManager : NetworkBehaviour
{

    public GameObject scoreText;
    public GameObject replayButton;
    public int score = 0;
    private int bricksDestroyed;

    // Start is called before the first frame update
    void Start()
    {
        replayButton.gameObject.SetActive(false);
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
          
        if(isServer) Replay();
    }



    public void Replay()
    {        
        bricksDestroyed = 0;
        replayButton.gameObject.SetActive(false);
        RpcUpdateScore(0);
    }

    public void UpdateScore()
    {
        bricksDestroyed++;
        score = score + 100;

        CmdUpdateScore(score);
        
        if(bricksDestroyed >= 50)
        {
            replayButton.gameObject.SetActive(true);
        }
    }

    [Command(requiresAuthority = false)]
    public void CmdUpdateScore(int cmdScore)
    {
        RpcUpdateScore(cmdScore);
    }

    [ClientRpc]
    public void RpcUpdateScore(int RpcScore)
    {
        scoreText.GetComponent<UnityEngine.UI.Text>().text = "Score: " + RpcScore;
    }
}
